<!DOCTYPE html>
<html>
<head>
	<title>QR CODE</title>
	<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
</head>
<body>
<h1>Qr Entry</h1>

<label>Enter Text To Generate QR-Code</label>
<br>
<textarea onkeyup="generate_qrcode(this.value)" cols="50" rows="5"></textarea>
<div id="result"></div>
</body>
<script>
	function generate_qrcode(sample){
		$.ajax({
			type:'post',
			url:'<?php echo base_url(); ?>Qrcode_module/generator',
			data:{sample:sample},
			success: function(code){
				$('#result').html(code);
			}
		});
	}
</script>
</html>