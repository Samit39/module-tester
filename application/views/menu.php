<!DOCTYPE html>
<html>
<head>
	<title>MENU</title>
</head>

<!-- ALERT CHECK GARNE CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" integrity="sha256-k66BSDvi6XBdtM2RH6QQvCz2wk81XcWsiZ3kn6uFTmM=" crossorigin="anonymous" />
<!-- ALERT CHECK GARNE CSS ENDS HERE -->

<body>
	<a href="pdf_module">Task_01 -> PDF GENERATION</a> <br>
	<a href="qrcode_module">Task_02 -> QR CODE GENERATION</a><br>
	<a href="#" id="checkalert">Alert check</a><br>
	<a href="html_to_pdf_module">Task_03 -> HTML PAGE TO PDF</a>

</body>
<script type="text/javascript" src="assets/js/jquery.js"></script>

<!-- ALERT CHECK GARNE SCRIPTS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" integrity="sha256-egVvxkq6UBCQyKzRBrDHu8miZ5FOaVrjSqQqauKglKc=" crossorigin="anonymous"></script>
<script type="text/javascript" src="assets/js/alertCheck.js"></script>
<!-- ALERT CHECK GARNE SCRIPTS ENDS HERE-->
</html>