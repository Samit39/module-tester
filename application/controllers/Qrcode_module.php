<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qrcode_module extends CI_Controller {

	public function __construct()
  {
    parent::__construct();
    // $this->load->helper('pdf_helper');

   }

   public function index(){
   	$this->load->view('Qrcode_generator/qrentry');
   }

   public function generator(){
   	$code='<center><img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl='.$_REQUEST['sample'].'" title="Link To Google.com"></center>';
	echo $code;
   }
}