<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Html_to_pdf_module extends CI_Controller {

	public function __construct()
  {
    parent::__construct();
    // $this->load->helper('pdf_helper');
   }

   public function index(){
   	$this->load->view('Html_To_Pdf_Generator/Html_to_pdf_generator');
   }
}