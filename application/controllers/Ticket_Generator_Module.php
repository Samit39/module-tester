<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket_Generator_Module extends CI_Controller {

	public function __construct()
  {
    parent::__construct();
    $this->load->helper('pdf_helper');

   }

	public function index(){
		$this->load->view('Ticket_Generator/Detail_insert');
	}
	public function generate_ticket(){
		$name=$this->input->post('name');
		$email=$this->input->post('email');
		$phone=$this->input->post('phone');
		// $data['todo_list'] = array('First Object', 'Second Object', 'Third Object');
		$data['desc'] = array(
				$name,
				$email,
				$phone
		);
		$data=array(
			'name'=>$name,
			'email'=>$email,
			'phone'=>$phone
		);
		// echo($title);
		// print_r($data);
		$this->load->view('Ticket_Generator/Pdf_generate',$data);
	}

	public function Generatepdf(){
		$name=$this->input->post('name');
		$email=$this->input->post('email');
		$phone=$this->input->post('phone');
		// $data['todo_list'] = array('First Object', 'Second Object', 'Third Object');
		$data=array(
			'name'=>$name,
			'email'=>$email,
			'phone'=>$phone
		);
		// echo($title);
		print_r($data);
		$this->load->view('Ticket_Generator/Final_Pdf_generate',$data);
	}
}